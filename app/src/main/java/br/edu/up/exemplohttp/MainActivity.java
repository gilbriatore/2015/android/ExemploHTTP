package br.edu.up.exemplohttp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class MainActivity extends AppCompatActivity {

  private ImageView imageView;
  private MediaPlayer mediaPlayer;
  private VideoView videoView;
  private final String GDRIVE = "https://drive.google.com/uc?export=view&id=";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    imageView = (ImageView) findViewById(R.id.imageView);
    videoView = (VideoView) findViewById(R.id.videoView);

  }

  public void onClickBaixarTexto(View v){

    BaixarTextoTask task = new BaixarTextoTask();
    task.execute();

  }

  /**
   * Classe responsável por fazer a requisição HTTP ao servidor
   * e processar o arquivo de texto baixado. O processamento ocorre
   * de forma assíncrona ou seja, não fica travando a tela.
   */
  public class BaixarTextoTask extends AsyncTask<String, String, String> {


    @Override
    protected String doInBackground(String... params) {

      try{

        String txt = "0BwO0-hA9fDiUMTFVS1NkVVhjaFk";
        URL url = new URL(GDRIVE + txt);

        //Abre a conexão com o servidor e faz uma requisição GET.
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.connect();

        //Faz a leitura dos dados retornados no stream;
        InputStream is = con.getInputStream();
        Scanner leitor = new Scanner(is);
        while (leitor.hasNext()){
          String linha = leitor.nextLine();
          Log.d("TXT", linha);
        }
        leitor.close();
        is.close();

//        //Faz a leitura dos dados retornados no stream;
//        InputStream is = con.getInputStream();
//        InputStreamReader isr = new InputStreamReader(is);
//
//        int tamanho = 0;
//        char[] buffer = new char[1024];
//        String texto = new String();
//        while((tamanho = isr.read(buffer)) != -1){
//          texto += String.copyValueOf(buffer, 0, tamanho);
//        }
//        Log.d("TXT", texto);
//        isr.close();
//        is.close();

//        //Faz a leitura dos dados retornados no stream;
//        InputStream is = con.getInputStream();
//        InputStreamReader isr = new InputStreamReader(is);
//        BufferedReader br = new BufferedReader(isr);
//
//        String linha = br.readLine();
//        while (linha != null){
//          Log.d("TXT", linha);
//          linha = br.readLine();
//        }
//        br.close();
//        isr.close();
//        is.close();

      } catch (Exception e){
        e.printStackTrace();
      }
      return null;
    }
  }

  public void onClickBaixarXML(View v){

    BaixarXMLTask task = new BaixarXMLTask();
    task.execute();

  }

  /**
   * Classe responsável por fazer a requisição HTTP ao servidor
   * e processar o arquivo XML baixado. O processamento ocorre
   * de forma assíncrona ou seja, não fica travando a tela.
   */
  public class BaixarXMLTask extends AsyncTask<String, String, String>{

      /*
        Exemplo dos dados retornados do XML;

        <ROW>
          <id>1</id>
          <nome>Alan</nome>
          <rg>25.253.394-X</rg>
          <cpf>213.909.848-00</cpf>
          <estado_civil>Solteiro</estado_civil>
          <sexo>M</sexo>
          <data_nasc>1988-09-06 00:00:00</data_nasc>
          <local_nasc>São José do Rio Preto</local_nasc>
        </ROW>
     */

    @Override
    protected String doInBackground(String... params) {

      try {

        String xml = "0BwO0-hA9fDiURlFzM3J5LTU2T1k";
        URL url = new URL(GDRIVE + xml);

        //Abre a conexão com o servidor e faz uma requisição GET.
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.connect();

        //Faz a leitura dos dados retornados no stream;
        InputStream is = con.getInputStream();
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(is);
        doc.getDocumentElement().normalize();

        NodeList itens = doc.getElementsByTagName("ROW");
        for (int i = 0; i < itens.getLength(); i++) {
          Node item = itens.item(i);
          if (item.getNodeType() == Node.ELEMENT_NODE){

            Element element = (Element) item;
            Node node = element.getElementsByTagName("id").item(0);
            Log.d("XML", "ID: " + node.getTextContent());

            node = element.getElementsByTagName("nome").item(0);
            Log.d("XML", "Nome: " + node.getTextContent());

            node = element.getElementsByTagName("rg").item(0);
            Log.d("XML", "RG: " + node.getTextContent());

            node = element.getElementsByTagName("cpf").item(0);
            Log.d("XML", "CPF: " + node.getTextContent());

            node = element.getElementsByTagName("estado_civil").item(0);
            Log.d("XML", "Estado civil: " + node.getTextContent());

            node = element.getElementsByTagName("sexo").item(0);
            Log.d("XML", "Sexo: " + node.getTextContent());

            node = element.getElementsByTagName("data_nasc").item(0);
            Log.d("XML", "Data de nascimento: " + node.getTextContent());

            node = element.getElementsByTagName("local_nasc").item(0);
            Log.d("XML", "Local de nascimento: " + node.getTextContent());
          }
        }
        is.close();

      } catch (Exception e) {
        e.printStackTrace();
      }

      return null;
    }
  }

  public void onClickBaixarJson(View v){

    BaixarJsonTask task = new BaixarJsonTask();
    task.execute();

  }

  /**
   * Classe responsável por fazer a requisição HTTP ao servidor
   * e processar o arquivo JSON baixado. O processamento ocorre
   * de forma assíncrona ou seja, não fica travando a tela.
   */
  public class BaixarJsonTask extends AsyncTask<String, String, String>{

    @Override
    protected String doInBackground(String... params) {

      try {

        String json = "0BwO0-hA9fDiUZU00cjdQUnNVckk";
        URL url = new URL(GDRIVE + json);

        //Abre a conexão com o servidor e faz uma requisição GET.
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.connect();

        //Faz a leitura dos dados retornados no stream;
        InputStream is = con.getInputStream();
        InputStreamReader isr = new InputStreamReader(is, "UTF-8");
        JsonReader reader = new JsonReader(isr);
        reader.beginArray();

        while (reader.hasNext()) {
          reader.beginObject();
          while (reader.hasNext()) {
            String campo = reader.nextName();
            String valor = reader.nextString();
            Log.d("JSON", campo + ": " + valor);
          }
          reader.endObject();
        }

        reader.endArray();
        reader.close();
        isr.close();
        is.close();

      } catch (Exception e) {
        e.printStackTrace();
      }

      return null;
    }
  }

  public void onClicarTocarMusica(View v){

    try {

      String musica = "0BwO0-hA9fDiUQ0EyTzdCelR3QlE";

      //O MediaPlayer se encarrega de fazer a requisição
      //ao servidor e fazer o stream do áudio.
      mediaPlayer = new MediaPlayer();
      mediaPlayer.setDataSource(GDRIVE + musica);
      mediaPlayer.prepare();
      mediaPlayer.start();

    } catch (Exception e) {
      e.printStackTrace();
    }

  }

  public void onClickTocarVideo(View v){

    imageView.setVisibility(View.INVISIBLE);
    videoView.setVisibility(View.VISIBLE);
    String video = "0BwO0-hA9fDiUZ2t5OTdUY3Z4ZEk";

    //O VideoView se encarrega de fazer a requisição
    //ao servidor e fazer o stream do vídeo.
    VideoView videoView = (VideoView) findViewById(R.id.videoView);
    videoView.setVideoPath(GDRIVE + video);
    videoView.setMediaController(new MediaController(this));
    videoView.start();

  }

  @Override
  protected void onStop() {
    super.onStop();

    if (mediaPlayer != null){
      mediaPlayer.stop();
      mediaPlayer.release();
      mediaPlayer = null;
    }

    if (videoView != null){
      videoView.stopPlayback();
      videoView = null;
    }
  }

  public void onClickMostrarImagem(View v){

    videoView.setVisibility(View.INVISIBLE);
    imageView.setVisibility(View.VISIBLE);

    String imagem = "0BwO0-hA9fDiUZjdnNVdvT3ZXWG8";
    BaixarImagemTask downloadTask = new BaixarImagemTask();
    downloadTask.execute(GDRIVE + imagem);

  }

  /**
   * Classe responsável por fazer a requisição HTTP ao servidor
   * e baixar a imagem. O download ocorre de forma assíncrona,
   * ou seja não fica travando a tela.
   */
  private class BaixarImagemTask extends AsyncTask<String, String, Bitmap> {


    @Override
    protected Bitmap doInBackground(String... params) {

      String str = params[0];
      Bitmap bitmap = null;
      try {

        URL url = new URL(str);

        Log.d("IMAGEM", "URL: " + str);

        //Abre a conexão com o servidor e faz uma requisição GET.
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.connect();

//        //Faz a leitura dos dados no stream,
//        InputStream is = con.getInputStream();
//        File sdcard = Environment.getExternalStorageDirectory();
//        File arquivo = new File(sdcard, "imagem.jpg");
//        FileOutputStream fos = new FileOutputStream(arquivo);
//
//        //Grava no armazenamento externo byte a byte;
//        int byteLido;
//        while ((byteLido = is.read()) != -1){
//          fos.write(byteLido);
//        }
//        fos.close();
//        is.close();
//
//        //Lê os dados do armazenamento externo;
//        FileInputStream fis = new FileInputStream(arquivo);
//        bitmap = BitmapFactory.decodeStream(fis);
//        fis.close();

        //Faz a leitura dos dados no stream,
        InputStream is = con.getInputStream();
        File sdcard = Environment.getExternalStorageDirectory();
        File arquivo = new File(sdcard, "imagem.jpg");
        FileOutputStream fos = new FileOutputStream(arquivo);
        BufferedOutputStream bos = new BufferedOutputStream(fos);

        // Grava a imagem no armazenamento externo usando
        // um buffer de 1k (1024 bytes) para cada loop.
        int bytesLidos = 0;
        byte[] buffer = new byte[1024];

        while ((bytesLidos = is.read(buffer)) > 0){
          bos.write(buffer, 0, bytesLidos);
        }
        bos.close();
        is.close();

        FileInputStream fis = new FileInputStream(arquivo);
        bitmap = BitmapFactory.decodeStream(fis);
        fis.close();

      } catch (Exception e){
        e.printStackTrace();
      }
      return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
      super.onPostExecute(bitmap);
      //Mostra a imagem na tela;
      imageView.setImageBitmap(bitmap);
    }
  }
}